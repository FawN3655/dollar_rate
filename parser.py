import requests
from bs4 import BeautifulSoup
from env import ENV


class Parser:
    @staticmethod
    def get_response() -> bool or bytes:
        """Получение ответа от сервера"""
        try:
            response = requests.get(ENV.requests_url, headers=ENV.requests_header, timeout=30)
            if response.status_code != 200:
                return False
            return response.content
        except (TimeoutError, ConnectionError):
            return False

    @staticmethod
    def parse_response(content: bytes) -> (int, int):
        """Парсинг данных из ответа"""
        soup = BeautifulSoup(content, "html5lib")
        au_mid_buysell = soup.find("div", class_="au-mid-buysell")
        if not au_mid_buysell:
            return False
        small = au_mid_buysell.find("small")
        if small:
            small.decompose()
        text = au_mid_buysell.text
        if not text:
            return False
        text = text.strip()
        text = text.rstrip(" грн")
        split_text = text.split(",")
        if len(split_text) != 2:
            return False
        first, second = split_text
        return int(first), int(second[:2])  # округление до двухзначного числа максимум

    def get_rates(self) -> (int, int):
        """Получение курса"""
        content = self.get_response()
        if not content:
            return False
        return self.parse_response(content)
