from sqlite3 import connect
from env import ENV


class DatabaseController:
    def __init__(self):
        self.connection = connect(ENV.database_path)
        self.cursor = self.connection.cursor()
        self.create_table()
        super(DatabaseController, self).__init__()

    def create_table(self):
        """Создание таблицы если такой нет"""
        self.cursor.execute("CREATE TABLE IF NOT EXISTS data_history (id INTEGER PRIMARY KEY, "
                            "create_time timestamp, val float);")
        self.connection.commit()

    def insert_new_data(self, value: float):
        """Добавление новой записи"""
        self.cursor.execute("INSERT INTO data_history(create_time, val) VALUES(CURRENT_TIMESTAMP, ?);", [value])
        self.connection.commit()

    def get_value_of_day(self, date: str) -> list:
        """Выборка по дате"""
        self.cursor.execute("SELECT TIME(create_time), val FROM data_history WHERE DATE(create_time)=? "
                            "ORDER BY create_time;", [date])
        result, __ = list(), list()
        for i in self.cursor.fetchall():
            time, value = i
            h, m, s = time.split(":")
            if h not in __:
                __.append(h)
                result.append(("{}:{}".format(h, m), value))
        return result[:ENV.plot_times_count_limit]

    def get_value_of_week(self, number_of_week: int) -> list:
        """Выборка по номеру недели
        +1 в выборки по причине того что во всем нормальном мире недели начинаются с первой, и только в sqlite с 0"""
        self.cursor.execute("SELECT create_time, val FROM data_history WHERE strftime('%W',create_time)+1=? "
                            "ORDER BY create_time;", [number_of_week])
        result, __ = list(), list()
        for i in self.cursor.fetchall():
            timestamp, value = i
            date, time = timestamp.split(" ")
            if date not in __:
                __.append(date)
                result.append(("{}\n{}\n".format(*reversed(date.split("-", 1))), value))
        return result

    def get_value_of_month(self, date: str) -> list:
        """Выборка по году и месяцу"""
        self.cursor.execute("SELECT create_time, val FROM data_history WHERE strftime('%Y-%m',create_time)=? "
                            "ORDER BY create_time;", [date])
        result, __ = list(), list()
        for i in self.cursor.fetchall():
            timestamp, value = i
            date, time = timestamp.split(" ")
            month, day = date.rsplit("-", 1)
            if date not in __:
                __.append(date)
                result.append((day, value))
        return result

    def get_value_of_year(self, date: str) -> list:
        """Выборка по году"""
        self.cursor.execute("SELECT create_time, val FROM data_history WHERE strftime('%Y',create_time)=? "
                            "ORDER BY create_time;", [date])
        result, __ = list(), list()
        for i in self.cursor.fetchall():
            timestamp, value = i
            date, time = timestamp.split(" ")
            year_and_month, day = date.rsplit("-", 1)
            if year_and_month not in __:
                __.append(year_and_month)
                result.append(("{}\n{}\n".format(*reversed(date.split("-", 1))), value))
        return result
