from os.path import join, dirname
from os import listdir
import json


class ENV:
    data_path = join(dirname(__file__), "data")
    fonts_path = join(data_path, "fonts")
    icons_path = join(data_path, "fonts")
    main_icon_path = join(icons_path, "icon.png")
    languages_path = join(data_path, "languages")
    database_path = join(data_path, "database.sqlite")
    default_language = "ru.json"
    languages = {i.split(".")[0]: i for i in listdir(languages_path)}
    window_geometry = [600, 450]  # геометрия основного окна приложения
    update_time = 15  # период обновления данных в минутах
    ms_constant = 1000 * 60  # константа для перевода милисекунд в минуты
    new_icon_size = [40, 40]  # размер новой иконки трея
    font_size = 20
    font_style = "ArialBoldItalic.ttf"
    font_style_path = join(fonts_path, font_style)
    icon_font_color = (255, 255, 255)
    icon_background_color = (0, 0, 0, 0)
    result_dollars_count = 3000
    schedule_margin = 5
    space_for_button = 50
    buttons_h = 40
    buttons_margin = 5
    plot_times_count_limit = 10

    requests_url = "https://minfin.com.ua/currency/auction/usd/buy/zaporozhye/"
    requests_header = {
        'authority': 'minfin.com.ua',
        'pragma': 'no-cache',
        'cache-control': 'no-cache',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'sec-fetch-site': 'cross-site',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-user': '?1',
        'sec-fetch-dest': 'document',
        'referer': 'https://www.google.com/',
        'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
    }

    def get_current_localisation(self, lang: str) -> dict:
        """
        Получение выбранной локализации
        :param lang: язык локализации, к примеру ru или en
        :return: json с выбранной локализацией
        """
        with open(join(self.languages_path, self.languages.get(lang, self.default_language))) as localisation_file:
            localisation = localisation_file.read()
        return json.loads(localisation)
