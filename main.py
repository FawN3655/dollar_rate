import sys
from datetime import datetime
import matplotlib
from PIL import Image, ImageDraw, ImageFont
from PIL.ImageQt import ImageQt
from PyQt5.QtCore import QTimer, QRect
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QSystemTrayIcon, QMenu, QApplication, QAction, QMainWindow, QPushButton
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure
from database_controller import DatabaseController
from env import ENV
from parser import Parser

matplotlib.use('Qt5Agg')


class MplCanvas(FigureCanvasQTAgg):
    """Инициализация кастомного графика"""

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)


class ApplicationUI(QMainWindow, Parser):
    schedules_view, exit = None, False

    def __init__(self):
        super(ApplicationUI, self).__init__()
        self.db = DatabaseController()
        self.ENV = ENV()
        self.localisation = self.ENV.get_current_localisation("ru")
        self.init_ui()
        self.tray_icon, self.rates_action, self.rates_3k_action, self.show_or_hide_action = self.create_tray_icon()
        self.timer = self.create_timer()
        self.set_actual_info()
        self.btn_list = self.create_ui()
        self.day_btn, self.week_btn, self.month_btn, self.year_btn = self.btn_list
        self.update_data = self.set_value_by_day

    def update_btn_color(self, btn):
        """Обновление цветов кнпопок при выборе"""
        for b in self.btn_list:
            b.setStyleSheet("QPushButton {border: 1px solid black; color: black} "
                            "QPushButton:hover:!pressed {border: 1px solid red; color: black}")
        btn.setStyleSheet("QPushButton {border: 1px solid black; color: red} "
                          "QPushButton:hover:!pressed {border: 1px solid red; color: red}")

    def set_value_by_year(self):
        """Установка данных графика за год"""
        self.update_btn_color(self.year_btn)
        self.update_data = self.set_value_by_year
        dates, values = map(list, zip(*self.db.get_value_of_year(datetime.strftime(datetime.now(), "%Y"))))
        self.set_schedule_data(values, dates)

    def set_value_by_month(self):
        """Установка данных графика за месяц"""
        self.update_btn_color(self.month_btn)
        self.update_data = self.set_value_by_month
        dates, values = map(list, zip(*self.db.get_value_of_month(datetime.strftime(datetime.now(), "%Y-%m"))))
        self.set_schedule_data(values, dates)

    def set_value_by_week(self):
        """Установка данных графика за неделю"""
        self.update_btn_color(self.week_btn)
        self.update_data = self.set_value_by_week
        dates, values = map(list, zip(*self.db.get_value_of_week(datetime.now().isocalendar()[1])))
        self.set_schedule_data(values, dates)

    def set_value_by_day(self):
        """Установка данных графика за день"""
        self.update_btn_color(self.day_btn)
        self.update_data = self.set_value_by_day
        dates, values = map(list, zip(*self.db.get_value_of_day(datetime.strftime(datetime.now(), "%Y-%m-%d"))))
        self.set_schedule_data(values, dates)

    def set_schedule_data(self, x: list, y: list):
        """Установка новго значения на график"""
        self.schedules_view.axes.cla()
        self.schedules_view.axes.plot(y, x, '.-', color='r')
        self.schedules_view.draw()

    def init_ui(self):
        """
        Создание главного окна приложения
        """
        self.setWindowTitle(self.localisation.get("app_name", "app_name"))
        self.setWindowIcon(QIcon(self.ENV.main_icon_path))

    def create_timer(self):
        """`
        Создание coroutine
        :return обьек таймера
        """
        timer = QTimer(self)
        timer.timeout.connect(self.set_actual_info)
        timer.start(self.ENV.ms_constant * self.ENV.update_time)
        return timer

    def set_actual_info(self):
        """
        Обновление информации по таймеру
        """
        current_info = self.get_rates()
        if current_info:
            first, second = current_info
            self.set_actual_icon(first, second)
            float_value = float("{}.{}".format(first, second))
            self.db.insert_new_data(float_value)
            self.rates_action.setText(self.localisation.get("rates_one_dollar", "{}").format(float_value))
            self.rates_3k_action.setText(self.localisation.get("rates_3k_dollar", "{}={}").format(
                self.ENV.result_dollars_count, round(self.ENV.result_dollars_count * float_value, 2)))

    def set_actual_icon(self, first: int, second: int):
        """
        Создание и обновление новой иконки трея на основание полученных данных
        :param first: целое число
        :param second: остаток
        """
        new_icon_w, new_icon_h = self.ENV.new_icon_size
        img = Image.new('RGB', (new_icon_w, new_icon_h), color=self.ENV.icon_background_color)
        draw = ImageDraw.Draw(img)
        draw.text((5, -3), "{}.\n{}".format(first, second), fill=self.ENV.icon_font_color,
                  font=ImageFont.truetype(self.ENV.font_style_path, self.ENV.font_size))
        self.tray_icon.setIcon(QIcon(QPixmap.fromImage(ImageQt(img))))

    def create_tray_icon(self):
        """
        Создание иконки трея и контекстного меню для взаимодействия
        :return: обьект иконки системного трея
        """
        tray_icon = QSystemTrayIcon(self)
        tray_icon.setIcon(QIcon(self.ENV.main_icon_path))
        #  Создания меню
        tray_menu = QMenu()
        rates_action = QAction(self.localisation.get("rates_one_dollar", "{}").format(0), self)
        rates_3k_action = QAction(self.localisation.get("rates_3k_dollar", "{}={}").format(
            self.ENV.result_dollars_count, 0, 0), self)
        show_or_hide_action = QAction(self.localisation.get("show", "show"), self)
        quit_action = QAction(self.localisation.get("exit", "exit"), self)
        show_or_hide_action.triggered.connect(self.custom_show)
        quit_action.triggered.connect(self.exit_event)
        tray_menu.addAction(rates_action)
        tray_menu.addAction(rates_3k_action)
        tray_menu.addSeparator()
        tray_menu.addAction(show_or_hide_action)
        tray_menu.addAction(quit_action)
        tray_icon.setContextMenu(tray_menu)
        tray_icon.show()
        return tray_icon, rates_action, rates_3k_action, show_or_hide_action

    def custom_show(self):
        """Показать или скрыть статистику"""
        self.show_or_hide_action.setText(self.localisation.get("show", "show") if self.isVisible() else
                                         self.localisation.get("hide", "hide"))
        self.update_data()
        self.setVisible(not self.isVisible())

    def create_ui(self):
        """Создание UI"""
        self.setMinimumSize(*self.ENV.window_geometry)
        self.schedules_view = MplCanvas(self, width=5, height=4, dpi=100)
        self.setCentralWidget(self.schedules_view)
        self.setStyleSheet("background-color: white")
        # Создание кнопки обновления данных графика на день
        day_btn = QPushButton(self)
        day_btn.setText(self.localisation.get("bay_btn_text", "bay_btn_text"))
        day_btn.clicked.connect(self.set_value_by_day)
        # Создание кнопки обновления данных графика на неделю
        week_btn = QPushButton(self)
        week_btn.setText(self.localisation.get("week_btn_text", "week_btn_text"))
        week_btn.clicked.connect(self.set_value_by_week)
        # Создание кнопки обновления данных графика на месяц
        month_btn = QPushButton(self)
        month_btn.setText(self.localisation.get("month_btn_text", "month_btn_text"))
        month_btn.clicked.connect(self.set_value_by_month)
        # Создание кнопки обновления данных графика на год
        year_btn = QPushButton(self)
        year_btn.setText(self.localisation.get("year_btn_text", "year_btn_text"))
        year_btn.clicked.connect(self.set_value_by_year)
        return day_btn, week_btn, month_btn, year_btn

    def resize_schedule(self):
        """Изменение размера графика под размер ui"""
        x, y, w, h = self.geometry().getRect()
        self.schedules_view.setGeometry(QRect(self.ENV.schedule_margin, self.ENV.schedule_margin +
                                              self.ENV.space_for_button, w - (self.ENV.schedule_margin * 2), h -
                                              ((self.ENV.schedule_margin * 2) + self.ENV.space_for_button)))
        btn_w = (w - (self.ENV.buttons_margin * 4)) / 4
        self.day_btn.setGeometry(QRect(self.ENV.buttons_margin, self.ENV.buttons_margin, btn_w, self.ENV.buttons_h))
        self.week_btn.setGeometry(QRect((self.ENV.buttons_margin * 2) + btn_w, self.ENV.buttons_margin,
                                        btn_w - self.ENV.buttons_margin, self.ENV.buttons_h))
        self.month_btn.setGeometry(QRect((self.ENV.buttons_margin * 2) + (btn_w * 2), self.ENV.buttons_margin,
                                         btn_w - self.ENV.buttons_margin, self.ENV.buttons_h))
        self.year_btn.setGeometry(QRect((self.ENV.buttons_margin * 2) + (btn_w * 3), self.ENV.buttons_margin,
                                        btn_w, self.ENV.buttons_h))

    def exit_event(self):
        """Кастомный метод закрытия приложения"""
        self.exit = True
        self.close()

    def resizeEvent(self, event):
        """Системный метод изменения размера приложения"""
        self.resize_schedule()

    def closeEvent(self, event):
        """Системный метод закрытия приложения"""
        if self.exit:
            event.accept()
        else:
            self.hide()
            self.show_or_hide_action.setText(self.localisation.get("show", "show"))
            event.ignore()


def main():
    """
    Запуск приложения
    """
    app = QApplication(sys.argv)
    ex = ApplicationUI()
    ex.setVisible(True)
    ex.update_data()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
